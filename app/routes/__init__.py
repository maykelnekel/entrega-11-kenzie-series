from flask import Flask

def init_app(app: Flask):
    from app.routes.ka_series_route import ka_series_route
    ka_series_route(app)
    
    return app 