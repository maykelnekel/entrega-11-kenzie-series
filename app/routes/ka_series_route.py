from flask import Flask
from app.controllers.series_controller import create, select_by_id, series


def ka_series_route(app: Flask):

    app.get('/series')(series)
    app.get('/series/<int:serie_id>')(select_by_id)
    app.post('/series')(create)
