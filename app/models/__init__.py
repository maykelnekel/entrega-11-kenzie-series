from os import getenv
from dotenv import load_dotenv
import psycopg2
from flask import jsonify

load_dotenv()

configs = {
    "host": getenv("HOST"),
    "database": getenv("DATABASE"),
    "user": getenv("USER"),
    "password": getenv("PASSWORD")
}


def conn_cur():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    return conn, cur

def commit_and_close(conn, cur):
    conn.commit()
    cur.close()
    conn.close()

def create_table():
    conn, cur = conn_cur()

    cur.execute("""
        CREATE TABLE IF NOT EXISTS ka_series (
            id BIGSERIAL PRIMARY KEY,
            "serie" VARCHAR(100) NOT NULL UNIQUE,
            seasons INTEGER NOT NULL,
            released_date DATE NOT NULL,
            "genre" VARCHAR(50) NOT NULL,
            imdb_rating FLOAT NOT NULL
        );
    """)
    commit_and_close(conn, cur)
