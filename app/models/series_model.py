import pdb
from app.models import conn_cur, commit_and_close
from flask import jsonify

class Series:

    series_keys = ["serie", "seasons", "released_date", "genre", "imdb_rating"]

    def __init__(self, serie: str, seasons: int, released_date: str, genre: str, imdb_rating: float):
        self.serie = serie
        self.seasons = seasons
        self.released_date = released_date
        self.genre = genre
        self.imdb_rating = imdb_rating

    def crate(self):
        conn, cur = conn_cur()

        execution_command = """
            INSERT INTO
                ka_series
            (serie, seasons, released_date, genre, imdb_rating)
            VALUES
                (%s, %s, %s, %s, %s)
            RETURNING *;
        """
        values = list(self.__dict__.values())

        cur.execute(execution_command, values)

        inserted_serie = cur.fetchone()

        commit_and_close(conn, cur)

        return dict(zip(self.series_keys, inserted_serie))

    @staticmethod
    def series():
        conn, cur = conn_cur()

        cur.execute("""
            SELECT * FROM ka_series;
        """)

        series = cur.fetchall()
        commit_and_close(conn, cur)
        serie_found = [dict(zip(Series.series_keys, serie)) for serie in series]


        return jsonify(serie_found)

    @staticmethod
    def select_by_id(serie_id):
        # pdb.set_trace()
        series_keys = ["serie", "seasons", "released_date", "genre", "imdb_rating"]

        serie_id_string = str(serie_id)
        conn, cur = conn_cur()

        cur.execute("""
            SELECT
                *
            FROM
                ka_series
            WHERE
                id= (%s);
        """, (serie_id_string))

        serie = cur.fetchone()

        commit_and_close(conn, cur)
        
        serie_found = dict(zip(series_keys, serie))
        
        return jsonify(serie_found)
