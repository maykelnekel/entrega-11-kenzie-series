from flask import request
from flask.json import jsonify
from app.models.series_model import Series
from app.controllers.series_controller import Series

def series():
    return Series.series()


def select_by_id(serie_id):
    try:
        return Series.select_by_id(serie_id)
    except TypeError:
        return jsonify([]), 404


def create():
    data = request.get_json()
    serie = Series(**data)
    return serie.crate(), 201
