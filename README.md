Entrega - Kenzie Serie
Objetivo
Para essa entrega você criará um sistema para armazenamento e criação de séries, com objetivo de trabalhar seus conhecimentos de Flask, PostgreSQL e Psycopg2.

Preparativos
Você deverá seguir a seguinte estrutura de pastas:

nome_do_projeto
├── app
│   ├── __init__.py
│   ├── controllers
│   │   └── __init__.py
│   ├── models
│   │   └── __init__.py
│   └── routes
│       └── __init__.py
├── .gitignore
└── requirements.txt
Requisitos
Aviso!
Siga os endpoints, status code, assinatura da função e as especificações da database como o esperado.

Todos os exemplos de entrada e saída estão neste link.

Os retornos NÃO precisam seguir na mesma ordem apresentada.


Kenzie Serie
Database
Você deverá criar o seu banco de dados com seguinte padrão abaixo:

NOME DA TABELA: ka_series;
id: BIGSERIAL e PRIMARY KEY;
serie: VARCHAR(100), NOT NULL e UNIQUE;
seasons: INTEGER e NOT NULL;
released_date: DATE e NOT NULL;
genre: VARCHAR(50) e NOT NULL;
imdb_rating: FLOAT e NOT NULL.

POST /series
Especificações da rota:

Assinatura da função:
create().
Rotina deverá ser:
Criação da tabela no seu banco de dados caso ela não exista;
Inserção da série que foi mandada pela requisição na tabela do seu banco de dados;
Os valores de serie e genre deverá ser salvo no formato de título.
Retorno:
Um dicionário com as informações passadas pela requisição;
Status code 201 - CREATED

GET /series
Especificações da rota:

Assinatura da função:
series().
Rotina deverá ser:
Seleção de todos os dados da tabela.
Retorno:
Caso exista dados na tabela deverá retornar:
Uma lista de dicionários com o resultado da seleção feita;
Status code 200 - OK.
Caso não exista dados na tabela deverá retornar:
Uma lista vazia;
Status code 200 - OK.
Caso a tabela não exista:
Fazer a criação da tabela;
Retornar uma lista vazia;
Status code 200 - OK.

GET /series/<int:serie_id>
Especificações da rota:

Assinatura da função:
select_by_id().
Rotina deverá ser:
Seleção de um dado da tabela filtrado pelo id.
Retorno:
Caso exista dados na tabela deverá retornar:
Um dicionário de dicionário com o resultado da seleção feita.
Status code 200 - OK.
Caso não exista dados na tabela, ou o respectivo id deverá retornar:
Um dicionário;
Status code 404 - NOT FOUND.
Caso a tabela não exista:
Fazer a criação da tabela;
Retornar um dicionário;
Status code 404 - NOT FOUND.

Entregáveis

Link do repositório no GitLab e incluir o grupo ka-br-mar-2021-correcoes como reporter.